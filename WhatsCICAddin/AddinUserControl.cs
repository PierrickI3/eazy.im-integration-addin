﻿using ININ.Client.Common.Connection;
using ININ.Client.Common.Interactions;
using ININ.Diagnostics;
using ININ.IceLib.Connection;
using ININ.IceLib.Interactions;
using ININ.InteractionClient;
using ININ.InteractionClient.AddIn;
using ININ.InteractionClient.Messages.MiniMode;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Xml.Serialization;

using ConnectionState = ININ.Client.Common.Connection.ConnectionState;
using ConnectionStateChangedEventArgs = ININ.Client.Common.Connection.ConnectionStateChangedEventArgs;
using IInteraction = ININ.InteractionClient.AddIn.IInteraction;
using Interaction = ININ.IceLib.Interactions.Interaction;
using InteractionCapabilities = ININ.IceLib.Interactions.InteractionCapabilities;
using InteractionEventArgs = ININ.InteractionClient.AddIn.InteractionEventArgs;
using InteractionState = ININ.Client.Common.Interactions.InteractionState;

namespace WhatsCICAddin
{
    public partial class AddinUserControl : UserControl, IWindow
    {
        #region Fields

        private IInteraction _currentInteraction;
        private IQueue _iQueue;
        private SynchronizationContext _synchronizationContext;
        private readonly List<IInteraction> _alreadyProcessedInteractions = new List<IInteraction>();
        private Session _session;
        private InteractionsManager _interactionsManager;
        private ISelectedInteractionProvider _selectedInteractionProvider;
        private IConnection _connection;
        //private IMessageBroker _messageBroker;
        //private readonly List<WebBrowserForm> _openedWebBrowserForms = new List<WebBrowserForm>();

        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer(string.Format("{0}.Addin", Resource1.AddinName));
        private static readonly ITopicTracerContextAttribute<string> InteractionIdAttribute = TopicTracerContextAttributeFactory.CreateStringContextAttribute(string.Format("{0}.Addin.InteractionId", Resource1.AddinName), "Interaction ID", "{}");

        private ITraceContext _traceContext;

        #endregion

        #region IWindow Members

        public event PropertyChangedEventHandler PropertyChanged;
        public string Title { get { return Resource1.AddinName; } }
        public object Content { get { return this; } }

        #endregion

        #region Constructor & Initialization

        public AddinUserControl()
        {
            InitializeComponent();
            Initialize();
        }

        public void Initialize()
        {
            try
            {
                _traceContext = ServiceLocator.Current.GetInstance<ITraceContext>();
                
                _traceContext.Verbose("Getting synchronization context.");
                _synchronizationContext = SynchronizationContext.Current;
                var myDomain = AppDomain.CurrentDomain;

                //_traceContext.Verbose("Selecting empty tab.");
                //_synchronizationContext.Send(state => mainWizardForm.SelectedTab = tpNoSelection, null);

                _traceContext.Verbose("Getting IConnection service.");
                _connection = ServiceLocator.Current.GetInstance<IConnection>();
                _connection.StateChanged += OnConnectionChanged;

                //_traceContext.Verbose("Getting IMessageBroker service.");
                //_messageBroker = ServiceLocator.Current.GetInstance<IMessageBroker>();

                //TODO Re-enable
                //_messageBroker.Subscribe<ApplicationExitMessage>(ApplicationExiting);

                //_traceContext.Verbose("Getting ISelectionInteraction service.");
                //_selectedInteractionProvider = ServiceLocator.Current.GetInstance<ISelectedInteractionProvider>();
                //_selectedInteractionProvider.SelectedInteractionChanged += SelectedInteractionChanged;

                _traceContext.Verbose("Getting session.");
                _session = ServiceLocator.Current.GetInstance<Session>();

                if (_session == null)
                {
                    _traceContext.Error("IceLib session is unavailable. Can't continue.");
                    return;
                }

                _traceContext.Verbose("Getting Interactions Manager instance.");
                _interactionsManager = InteractionsManager.GetInstance(_session);
                _interactionsManager.PerformingAction += OnPerformingAction;
                _interactionsManager.PerformedAction += OnPerformedAction;

                var attributes = new List<string>()
                {
                    InteractionAttributeName.State,
                    "WhatsCIC_Text"
                };

                //var monitoredAttributes = new List<string>();
                //monitoredAttributes.AddRange(CommonAttributes.MonitoredAttributes.AllAttributes);

                var supportingAttributes = new List<string>();
                supportingAttributes.AddRange(CommonAttributes.SupportingAttributes.AllAttributes);

                _traceContext.Note(string.Format("Getting IQueue... Watching {0} attributes and {1} supporting attributes", attributes.Count, supportingAttributes.Count));

                _iQueue = ServiceLocator.Current.GetInstance<IQueueService>().GetMyInteractions(attributes);//, supportingAttributes);
                _iQueue.InteractionAdded += OnInteractionAdded;
                _iQueue.InteractionChanged += OnInteractionChanged;
                _iQueue.InteractionRemoved += OnInteractionRemoved;

                InitializeTab();

                _traceContext.Always("WhatsCIC Control has successfully initialized.");

                AddToChat("Hello!", "System");
                AddToChat("Welcome to WhatsCIC...", "System");
            }
            catch (Exception ex)
            {
                _traceContext.Exception(ex, string.Empty);
            }
        }

        private void ApplicationExiting(ApplicationExitMessage applicationExitMessage)
        {
            _traceContext.Always("Client is exiting. Saving current conversations.");
            SaveConversation();
        }

        private void OnConnectionChanged(object sender, ConnectionStateChangedEventArgs connectionStateChangedEventArgs)
        {
            _traceContext.Note(string.Format("CIC Connection has changed from {0} to {1}. Message: {2}, Reason: {3}.", connectionStateChangedEventArgs.PreviousState, connectionStateChangedEventArgs.CurrentState, connectionStateChangedEventArgs.Message, connectionStateChangedEventArgs.Reason));
            if (connectionStateChangedEventArgs.CurrentState == ConnectionState.Down)
            {
                _traceContext.Warning("CIC connection is down. Clearing tabs.");
                ClearTabs();
            }
        }

        #endregion

        #region Interactions

        private void OnInteractionAdded(object sender, InteractionEventArgs interactionEventArgs)
        {
            _traceContext.Note(string.Format("Interaction Added: {0}", interactionEventArgs.Interaction.InteractionId));
            OnInteractionChanged(this, interactionEventArgs);
        }

        private void OnInteractionChanged(object sender, InteractionEventArgs interactionEventArgs)
        {
            try
            {
                var interaction = interactionEventArgs.Interaction;
                _currentInteraction = interaction;
                _traceContext.Note(string.Format("Interaction Changed: {0}", interaction.InteractionId));

                #region Validation

                _traceContext.Note("Checking if the interaction is disconnected.");
                if (interaction.GetAttribute(InteractionAttributeName.State).Equals(InteractionAttributeValues.State.ExternalDisconnect) ||
                    interaction.GetAttribute(InteractionAttributeName.State).Equals(InteractionAttributeValues.State.InternalDisconnect))
                {
                    _traceContext.Note(string.Format("Interaction {0} is disconnected.", interaction.InteractionId));
                    if (GetNumberOfConnectedInteractions() <= 1)
                    {
                        _traceContext.Note("There are no connected interactions. Calling ClearTabs().");
                        ClearTabs();
                    }
                    return;
                }

                _traceContext.Note("Checking if the interaction is connected.");
                if (!interaction.GetAttribute(InteractionAttributeName.State).Equals(InteractionAttributeValues.State.Connected))
                {
                    _traceContext.Note(string.Format("Interaction {0} is not connected.", interaction.InteractionId));
                    if (GetNumberOfConnectedInteractions() <= 1)
                    {
                        _traceContext.Note("There are no connected interactions. Calling ClearTabs().");
                        ClearTabs();
                    }
                    return;
                }

                _traceContext.Note("Adding interaction to the interactions list.");
                AddToInteractionsList(interaction);

                #endregion

                _traceContext.Note("Updating chat window");
                _synchronizationContext.Send(state => UpdateChat(interaction), null);
            }
            catch (Exception ex)
            {
                _traceContext.Exception(ex, string.Empty);
            }
        }

        private void OnInteractionRemoved(object sender, InteractionEventArgs interactionEventArgs)
        {
            try
            {
                var interaction = interactionEventArgs.Interaction;
                _traceContext.Note(string.Format("Interaction Removed: {0}", interaction.InteractionId));

                #region Close any associated web browser form

                /*
                lock (_openedWebBrowserForms)
                {
                    var listOfRemovedForms = new List<WebBrowserForm>();
                    foreach (var openedWebBrowserForm in
                        from openedWebBrowserForm in _openedWebBrowserForms
                        let associatedInteraction = openedWebBrowserForm.Tag as IInteraction
                        where associatedInteraction.InteractionId.Equals(interaction.InteractionId)
                        select openedWebBrowserForm)
                    {
                        openedWebBrowserForm.Close();
                        openedWebBrowserForm.Dispose();
                        listOfRemovedForms.Add(openedWebBrowserForm);
                    }

                    foreach (var removedForm in listOfRemovedForms)
                    {
                        _openedWebBrowserForms.Remove(removedForm);
                    }
                }
                */

                #endregion

                RemoveFromInteractionsList(interaction);

                /*
                var tabInteraction = mainWizardForm.SelectedTab.Tag as IInteraction;
                if (tabInteraction != null && tabInteraction.InteractionId.Equals(interactionEventArgs.Interaction.InteractionId))
                {
                    _synchronizationContext.Send(o => mainWizardForm.SelectedTab = tpNoSelection, null);
                }
                */
            }
            catch (Exception ex)
            {
                _traceContext.Exception(ex, string.Empty);
            }

        }

        /*
        private void SelectedInteractionChanged(object sender, EventArgs eventArgs)
        {
            try
            {
                #region Get Selected Interaction

                var selectedInteraction = _selectedInteractionProvider.SelectedInteraction;
                if (selectedInteraction == null)
                {
                    _traceContext.Verbose("No interaction selected.");
                    return;
                }
                _traceContext.Note(string.Format("Interaction {0} is selected.", selectedInteraction.InteractionId));

                #endregion

                #region Selected Interaction is already displayed?

                
                //var displayedInteraction = mainWizardForm.SelectedTab.Tag as IInteraction;
                //if (displayedInteraction != null)
                //{
                //    if (selectedInteraction.InteractionId.Equals(displayedInteraction.InteractionId))
                //    {
                //        _traceContext.Note("Selected interaction is the same as the currently displayed interaction. No need to do anything.");
                //        return;
                //    }
                //    SaveConversation();
                //}
                
                #endregion

                #region Connected?

                if (selectedInteraction.InteractionState != InteractionState.Connected)
                {
                    _traceContext.Note(string.Format("Selected interaction {0} is not connected.", selectedInteraction.InteractionId));
                    ClearTabs();
                    return;
                }

                #endregion

                #region Handled Interaction?

                var interactionToPopulate = _alreadyProcessedInteractions.FirstOrDefault(alreadyProcessedInteraction => alreadyProcessedInteraction.InteractionId.Equals(selectedInteraction.InteractionId));
                if (interactionToPopulate == null)
                {
                    _traceContext.Note("Could not find IInteraction from selected interaction. This means the currently selected interaction is not handled by this add-in.");
                    return;
                }

                #endregion

                UpdateChat(interactionToPopulate);
            }
            catch (Exception ex)
            {
                _traceContext.Exception(ex, string.Empty);
            }

        }
        */

        private void OnPerformingAction(object sender, PerformingActionEventArgs performingActionEventArgs)
        {
            _traceContext.Note(string.Format("Performing action: {0} on interaction {0}.", performingActionEventArgs.Action, performingActionEventArgs.Interaction.InteractionId.Id));
            if (performingActionEventArgs.Action == InteractionCapabilities.Disconnect)
            {
                if (!CanInteractionBeDisconnected(performingActionEventArgs.Interaction.InteractionId.Id.ToString()))
                {
                    _traceContext.Warning(string.Format("Can't disconnect interaction {0} now.", performingActionEventArgs.Interaction.InteractionId.Id));
                    performingActionEventArgs.Cancel = true;
                    return;
                }
                performingActionEventArgs.Cancel = false;
            }
            else if (performingActionEventArgs.Action == InteractionCapabilities.Transfer)
            {
                _interactionsManager.AddWrapUpAssignment(new WrapUpAssignment(performingActionEventArgs.Interaction.InteractionId, performingActionEventArgs.Interaction.InteractionId, performingActionEventArgs.Interaction.LastSegmentForCurrentUser.SegmentId, "Transferred", DateTime.Now));
            }
        }

        private void OnPerformedAction(object sender, PerformedActionEventArgs e)
        {
            _traceContext.Note(string.Format("Action {0} performed on interaction {1}.", e.Action, e.Interaction.InteractionId.Id));
            if (e.Action == InteractionCapabilities.Transfer)
            {
                foreach (var parameter in e.ActionParameters)
                {
                    _traceContext.Note(string.Format("Action parameter: Name: {0}. Value: {1}.", parameter.Key, parameter.Value.ToString()));

                    //TODO Add wrap up assignment to show transfer?
                    //_interactionsManager.AddWrapUpAssignment(new WrapUpAssignment(iceLibInteraction.InteractionId, iceLibInteraction.InteractionId, iceLibInteraction.LastSegmentForCurrentUser.SegmentId, SocialInteractionsCommon.WRAP_UP_CODE_REPLIED_AWAIT_APPROVAL, DateTime.Now));
                }
            }
        }

        private bool CanInteractionBeDisconnected(string interactionId)
        {
            try
            {
                return true;
                /*
                var displayedInteraction = mainWizardForm.SelectedTab.Tag as IInteraction;
                if (displayedInteraction == null) return true;
                if (!displayedInteraction.InteractionId.Equals(interactionId)) return true;

                if (mainWizardForm.SelectedTab.Text.Equals("Twitter Agent"))
                {
                    return MessageBox.Show(this, "Are you sure you want to disconnect this tweet?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes;
                }
                */
                //TODO Handle other tabs here
            }
            catch (Exception ex)
            {
                _traceContext.Exception(ex, string.Empty);
            }
            return false;
        }

        private static void EnsureInteractionIsWatchingAttribute(Interaction interaction, string attributeName)
        {
            if (interaction.IsWatching(attributeName)) return;

            if (interaction.IsWatching())
            {
                interaction.ChangeWatchedAttributes(new[] { attributeName }, null, false);
            }
            else
            {
                interaction.StartWatching(new[] { attributeName });
            }
        }

        private int GetNumberOfConnectedInteractions()
        {
            if (_iQueue == null) return 0;
            if (_iQueue.Interactions == null) return 0;

            var listOfConnectedInteractions = _iQueue.Interactions.Where(interaction => interaction.GetAttribute(InteractionAttributeName.State).Equals(InteractionAttributeValues.State.Connected)).ToList();
            _traceContext.Note(string.Format("Number of connected interactions: {0}", listOfConnectedInteractions.Count));
            return listOfConnectedInteractions.Count;
        }

        #endregion

        #region Tabs

        private void InitializeTab()
        {
            _traceContext.Note("Initializing WhatsCIC Tab.");
            _synchronizationContext.Send(state =>
            {
                //TODO Clear fields
            }, null);
        }

        public void ClearTabs()
        {
            try
            {
                _traceContext.Note("Clearing all tabs.");
                _synchronizationContext.Send(o =>
                {
                    //mainWizardForm.SelectedTab = tpNoSelection;
                }, null);

                _traceContext.Verbose("Calling InitializeTab().");
                InitializeTab();

                _traceContext.Note("WhatsCIC Control has been successfully cleared.");
            }
            catch (Exception ex)
            {
                _traceContext.Exception(ex, string.Empty);
            }
        }

        private void UpdateChat(IInteraction interaction)
        {
            try
            {
                _traceContext.Note(string.Format("UpdateChat({0})", interaction.InteractionId));
                AddToChat(interaction.GetAttribute("WhatsCIC_Text"), interaction.GetAttribute("WhatsCIC_From"));
            }
            catch (Exception ex)
            {
                _traceContext.Exception(ex, string.Empty);
            }
        }

        #endregion

        private void SaveConversation()
        {

        }

        #region Chat

        private void AddToChat(string text, string sender)
        {
            rtbChat.AppendText("From ");
            rtbChat.AppendText(sender);
            rtbChat.AppendText(string.Format(": {0}", text));
            rtbChat.AppendText(Environment.NewLine);
        }
        
        #endregion

        #region Helpers

        private static T DeserializeFromXml<T>(string xml)
        {
            T result;
            var ser = new XmlSerializer(typeof(T));
            using (TextReader tr = new StringReader(xml))
            {
                result = (T)ser.Deserialize(tr);
            }
            return result;
        }

        private void AddToInteractionsList(IInteraction interaction)
        {
            lock (_alreadyProcessedInteractions)
            {
                if (!_alreadyProcessedInteractions.Contains(interaction))
                    _alreadyProcessedInteractions.Add(interaction);
            }
        }

        private void RemoveFromInteractionsList(IInteraction interaction)
        {
            lock (_alreadyProcessedInteractions)
            {
                if (_alreadyProcessedInteractions.Contains(interaction))
                    _alreadyProcessedInteractions.Remove(interaction);
            }
        }

        private bool IsInteractionAlreadyProcessed(IInteraction interaction)
        {
            return _alreadyProcessedInteractions.Contains(interaction);
        }

        private static string GetCurrentCultureDateTimeString(DateTime dateTime)
        {
            return String.Format("{0} {1}", dateTime.ToString(CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern), dateTime.ToString(CultureInfo.CurrentUICulture.DateTimeFormat.ShortTimePattern));
        }

        #endregion

        private void txtMessage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                // Send message
                SendMessageToServer(_currentInteraction, txtMessage.Text);

                // Show message in rtb
                AddToChat(txtMessage.Text, _currentInteraction.GetAttribute("Eic_LocalUserId"));

                // Clear message
                txtMessage.Clear();
            }
        }

        private void SendMessageToServer(IInteraction interaction, string message)
        {
            //TODO DO THIS ON A DIFFERENT THREAD. THIS IS BLOCKING THE UI.

            try
            {
                var url = interaction.GetAttribute("WhatsCIC_ReplyUrl");
                if (string.IsNullOrEmpty(url))
                {
                    Topic.Error("No URL on interaction");
                    return;
                }
                Topic.Verbose("Sending message {0} (interaction: {1}) to url: {2}", message, interaction.InteractionId, url);

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(new
                    {
                        text = message,
                        conversationId = interaction.GetAttribute("WhatsCIC_MessageId"),
                        to = interaction.GetAttribute("WhatsCIC_To")
                    });

                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Topic.Exception(ex);
            }
        }
    }
}
