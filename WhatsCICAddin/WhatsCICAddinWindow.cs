﻿using ININ.InteractionClient.AddIn;

namespace WhatsCICAddin
{
    public class WhatsCICAddinWindow: AddInWindow
    {
        protected override string Id { get { return Resource1.AddinName + "Id"; } }
        protected override string DisplayName { get { return Resource1.AddinDisplayName; } }
        protected override string CategoryId { get { return Resource1.AddinCategory; } }
        protected override string CategoryDisplayName { get { return Resource1.AddinCategoryDisplayName; } }
        public override object Content { get { return new AddinUserControl(); } }
    }
}