﻿using System.Collections.Generic;

namespace WhatsCICAddin
{
    public static class CommonAttributes
    {
        public static class MonitoredAttributes
        {
            public static List<string> AllAttributes = new List<string>(new string[] {
                State,
                MessageContent
            });
            
            public static string State = "Eic_State";
            public static string MessageContent = "WhatsCIC_Text";
        }

        public static class SupportingAttributes
        {
            public static List<string> AllAttributes = new List<string>(new string[] {
                From,
                Time
            });

            public static string From = "WhatsCIC_From";
            public static string Time = "WhatsCIC_Time";
        }
    }
    public static class WhatsCICAttributesValues
    {
        public static class MessageTypes
        {
            public static string Text = "text";
            public static string Audio = "audio";
            public static string Image = "image";
        }
    }
}
