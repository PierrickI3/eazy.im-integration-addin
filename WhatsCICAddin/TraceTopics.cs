﻿using ININ.Diagnostics;

namespace WhatsCICAddin
{
    public class WhatsCICTopic
    {
        private static readonly ITopicTracer Topic = TopicTracerFactory.CreateTopicTracer(string.Format("{0}.Addin", Resource1.AddinName));

        public WhatsCICTopic()
        {
#if DEBUG
            bool success = TopicTracerFactory.TrySetImplementation(new DebugTopicTracerFactoryImplementation());
#endif
        }
    }
}

